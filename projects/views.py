from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    proj1 = Project.objects.filter(owner=request.user)
    context = {"proj2": proj1}
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    pro1 = get_object_or_404(Project, id=id)
    pro3 = pro1.tasks.all()
    context = {"pro2": pro1, "pro4": pro3}
    return render(request, "projects/show_projects.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form1": form}
    return render(request, "projects/create_project.html", context)
