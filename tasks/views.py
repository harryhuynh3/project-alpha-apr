from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form2": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def show_task(request):
    tasks5 = Task.objects.filter(assignee=request.user)
    context = {"tasks6": tasks5}
    return render(request, "tasks/my_tasks.html", context)
